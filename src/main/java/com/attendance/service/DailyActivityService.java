package com.attendance.service;

import com.attendance.model.DailyActivity;
import com.attendance.repository.DailyActivityRepository;
import com.attendance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;

@Service
public class DailyActivityService {
    @Autowired
    private DailyActivityRepository dailyActivityRepository;
    @Autowired
    private UserRepository userRepository;

    public DailyActivity submitActivity(Long userId, String activity) {
        LocalDate today = LocalDate.now();
        LocalTime now = LocalTime.now();

        if (now.isBefore(LocalTime.of(17, 0))) {
            throw new IllegalStateException("Cannot submit activity before 17:00");
        }

        DailyActivity dailyActivity = new DailyActivity();
        dailyActivity.setUser(userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User not found")));
        dailyActivity.setActivity(activity);
        dailyActivity.setSubmittedAt(now);
        dailyActivity.setDate(today);

        return dailyActivityRepository.save(dailyActivity);
    }
}
