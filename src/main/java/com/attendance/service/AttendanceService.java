package com.attendance.service;

import com.attendance.model.Attendance;
import com.attendance.repository.AttendanceRepository;
import com.attendance.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Service
public class AttendanceService {

    @Autowired
    private AttendanceRepository attendanceRepository;

    @Autowired
    private UserRepository userRepository;

    public Attendance checkIn(Long userId) {
        LocalDate today = LocalDate.now();
        List<Attendance> attendances = attendanceRepository.findByUserIdAndDate(userId, today);

        if (!attendances.isEmpty()) {
            throw new IllegalStateException("User has already checked in today");
        }

        Attendance attendance = new Attendance();
        attendance.setUser(userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User not found")));
        attendance.setCheckIn(LocalTime.now());
        attendance.setDate(today);

        if (attendance.getCheckIn().isAfter(LocalTime.of(8, 0))) {
            attendance.setStatus(Attendance.Status.LATE);
        } else {
            attendance.setStatus(Attendance.Status.ON_TIME);
        }

        return attendanceRepository.save(attendance);
    }

    public Attendance checkOut(Long userId) {
        LocalDate today = LocalDate.now();
        List<Attendance> attendances = attendanceRepository.findByUserIdAndDate(userId, today);

        if (attendances.isEmpty()) {
            throw new IllegalStateException("User has not checked in today");
        }

        Attendance attendance = attendances.get(0);
        attendance.setCheckOut(LocalTime.now());

        if (attendance.getCheckOut().isBefore(LocalTime.of(17, 0))) {
            attendance.setStatus(Attendance.Status.EARLY_BACK);
        }

        return attendanceRepository.save(attendance);
    }

    public void updateAttendanceStatus() {
        LocalDate today = LocalDate.now();
        List<Attendance> attendances = attendanceRepository.findByDate(today);

        for (Attendance attendance : attendances) {
            if (attendance.getCheckIn() == null && attendance.getCheckOut() == null) {
                attendance.setStatus(Attendance.Status.ABSENT);
                attendanceRepository.save(attendance);
            }
        }
    }
}
