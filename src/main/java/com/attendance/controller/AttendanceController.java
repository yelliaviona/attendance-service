package com.attendance.controller;


import com.attendance.model.Attendance;
import com.attendance.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/attendances")
public class AttendanceController {
    @Autowired
    private AttendanceService attendanceService;

    @PostMapping("/checkin")
    public Attendance checkIn(@RequestParam Long userId) {
        return attendanceService.checkIn(userId);
    }

    @PostMapping("/checkout")
    public Attendance checkOut(@RequestParam Long userId) {
        return attendanceService.checkOut(userId);
    }
}
