package com.attendance.controller;

import com.attendance.model.DailyActivity;
import com.attendance.service.DailyActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/activities")
public class DailyActivityController {
    @Autowired
    private DailyActivityService dailyActivityService;

    @PostMapping("/submit")
    public DailyActivity submitActivity(@RequestParam Long userId, @RequestParam String activity) {
        return dailyActivityService.submitActivity(userId, activity);
    }
}
